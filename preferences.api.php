<?php

/**
 * @file
 * Preferences API documentation.
 */

/**
 * @addtogroup hooks
 * @{
 */

/**
 * Defines per-user preferences to add for a module.
 *
 * @return array
 *   A associative array of preferences definitions keyed by the preferences
 *   name:
 *   - label: (Optional) A human-readable name for this preferences store.
 *   - type: The data type for the preference defined as one of the primitive
 *     data types bool, int, float, string, or a any for a complex data type.
 *     Complex data types must define a schema defined below.
 *   - access_callback: (Optional) An access callback to restrict the access
 *     of these preferences. The callback will be provided the operation,
 *     preferences name, the preferences definition, the preferences object,
 *     the user account the preferences blong to and the user account
 *     requesting access. Access is only granted to the "administer user"
 *     permissions or the user's own preferences.
 *   - default_value: (Optional) A default value to use for primitive types.
 *     Otherwise this is unused and properties will have default values as
 *     defined below.
 *   - props: (Optional) An associative array of properties and property info
 *     keyed by the property name.
 *     - label: (Optionasl) A human-readable name for this property.
 *     - type: The data type: bool, int, float, string, any or an array
 *       of those types e.g. list<TYPE>.
 *     - property_validate: (Optional) An array of validation callbacks to use.
 *       Primitive types are validated automatically. Modules should define
 *       validation so that arbitrary data is not allowed to be stored, but
 *       there is the use case of having a "schema-less" object so it is not
 *       required. You should evaluate security risks before not requiring
 *       any back end validation. A validation callback should return TRUE or
 *       FALSE.
 *     - default_value: (Optional) A default value to use. Defaults to a
 *       nil-value (NULL).
 *     - default_value_callback: (Optional) A callback to use to provide the
 *       default value instead of using a static value above. The
 *       default_value key above is ignored if this is provided.
 *   - module: the module that owns this preference definition.
 */
function hook_preferences_info() {
  $info = [
    'example_primitive' => [
      'label' => t('Option example'),
      'module' => 'mymodule',
      'type' => 'bool',
    ],
    'example_complex' => [
      'label' => t('Complex example'),
      'module' => 'mymodule',
      'type' => 'any',
      'access_callback' => 'example_complex_preferences_access',
      'schema' => [
        'boolean_example' => [
          'label' => t('Boolean example'),
          'type' => 'bool',
          'default_value' => TRUE,
        ],
        'string' => [
          'label' => t('String example'),
          'type' => 'string',
          'default_value' => '',
          'property_validate' => ['valid_url'],
        ],
        'list_example' => [
          'label' => t('List example'),
          'type' => 'list<string>',
          'default_value' => [],
        ],
        'any_example' => [
          'label' => t('Complex example'),
          'type' => 'any',
          'property_validate' => ['example_complex_any_example_validate'],
          'default_value_callback' => 'example_complex_any_example_default',
        ],
      ],
    ],
  ];
  return $info;
}

/**
 * Allows modules to alter the preferences defined by another module.
 *
 * @param array &$info
 *   An array of preference definitions keyed by the preferences name.
 *
 * @see hook_preferences_info()
 */
function hook_preferences_info_alter(array &$info) {
  $info['global']['schema']['example_global'] = [
    'label' => t('Alter example'),
    'type' => 'bool',
    'default_value' => FALSE,
  ];
}

/**
 * @} End of "addtogroup hooks"
 */
