<?php

/**
 * @file
 * Preferences administration pages.
 */

/**
 * Preferences settings form.
 *
 * @param array $form
 *   The form array.
 * @param array &$form_state
 *   The form state array.
 *
 * @return array
 *   The form array.
 */
function preferences_settings_form(array $form, array &$form_state) {

  $form['preferences_services_attach_user'] = [
    '#type' => 'checkbox',
    '#title' => t('Attach preferences to user resource?'),
    '#description' => t("Attaches a user's preferences to the user object when returned as a part of a Services module request. This option may have performance implications, but makes it easier for API implementors to use preferences."),
    '#required' => TRUE,
    '#default_value' => variable_get('preferences_services_attach_user', 0),
  ];

  return system_settings_form($form);
}

