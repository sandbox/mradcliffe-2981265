<?php

/**
 * @file
 * Preferences definitions.
 */

/**
 * Implements hook_preferences_info().
 */
function preferences_preferences_info() {
  $definitions = [];

  // Adds empty global preferences definition.
  $definitions['global'] = [
    'label' => t('Global'),
    'type' => 'any',
    'props' => [],
    'module' => 'preferences',
  ];

  return $definitions;
}
