<?php

/**
 * @file
 * Preferences Test Pages.
 */

/**
 * Saves preferences for the provided user account.
 *
 * @param array $form
 *   The form array.
 * @param array &$form_state
 *   The form state array.
 * @param object $account
 *   (Optional) The user account to save preferences. Defaults to current user.
 */
function preferences_test_form(array $form, array &$form_state, $account = NULL) {
  global $user;

  if (!isset($account) || $account === NULL) {
    $account = NULL;
  }

  $form_state['account'] = $account;

  // Load the preferences.
  $preferences = preferences_load(NULL, $account);

  $form['preferences'] = [
    'favorite_color' => [
      '#type' => 'markup',
      '#markup' => isset($preferences['preferences_test']) && isset($preferences['preferences_test']['favorite_color']) ? $preferences['preferences_test']['favorite_color'] : 'No Favorite Color',
    ],
		'nullable_toggle' => [
      '#type' => 'markup',
      '#markup' => isset($preferences['preferences_test']) && isset($preferences['preferences_test']['nullable_toggle']) ? ($preferences['preferences_test']['nullable_toggle'] === TRUE ? 'Nullable Toggle: TRUE' : 'Nullable Toggle: FALSE') : 'No Nullable Toggle',
    ],
		'preferred_pronouns' => [
      '#type' => 'markup',
      '#markup' => isset($preferences['preferences_test']) && isset($preferences['preferences_test']['preferred_pronouns']) ? implode(',', $preferences['preferences_test']['preferred_pronouns']) : 'No Pronouns',
    ],
    'team' => [
      '#type' => 'markup',
      '#markup' => isset($preferences['preferences_test']) ? $preferences['preferences_test']['favorites']['team'] : 'No Favorite Team',
    ],
  ];

  $form['actions'] = ['#type' => 'actions'];
  $form['actions']['submit'] = [
    '#type' => 'submit',
    '#value' => t('Submit'),
  ];

  return $form;
}

/**
 * Preferences test form submit callback.
 *
 * @param array $form
 *   The form array.
 * @param array &$form_state
 *   The form state array.
 */
function preferences_test_form_submit(array $form, array &$form_state) {
  $account = $form_state['account'];

  try {
    $preferences = preferences_load(NULL, $account);
    $values = [
      'favorite_color' => $account === NULL ? 'blue' : 'green',
      'nullable_toggle' => $account === NULL,
      'preferred_pronouns' => $account === NULL ? ['they'] : ['zir'],
    ];
    $modified = preferences_set('preferences_test', $values, $preferences, $account);
    preferences_save($modified, $account);
  }
  catch (\InvalidArgumentException $e) {
    form_set_error('', 'Exception thrown');
  }

  watchdog('preferences_test', 'This should only be displayed for test runs');
}

