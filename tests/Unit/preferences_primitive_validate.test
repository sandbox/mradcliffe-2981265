<?php

/**
 * @file
 * Preferences Primitive Validate tests.
 */

/**
 * Tests the preferences_primitive_validate function.
 */
class PreferencesPrimitiveValidateTestCase extends DrupalUnitTestCase {

  /**
   * {@inheritdoc}
   */
  public static function getInfo() {
    return [
      'name' => 'Preferences Primitive Validate',
      'description' => 'Tests the preferences_primitive_validate function.',
      'group' => 'Preferences',
    ];
  }

  /**
   * Asserts integer.
   */
  public function testInt() {
    $this->assertTrue(preferences_primitive_validate('int', 5));
  }

  /**
   * Asserts not integer.
   */
  public function testNoInt() {
    $this->assertFalse(preferences_primitive_validate('int', 0.0));
  }

  /**
   * Asserts boolean.
   */
  public function testBool() {
    $this->assertTrue(preferences_primitive_validate('bool', FALSE));
  }

  /**
   * Asserts not boolean.
   */
  public function testNoBool() {
    $this->assertFalse(preferences_primitive_validate('bool', 'aoeui'));
  }

  /**
   * Asserts float.
   */
  public function testFloat() {
    $this->assertTrue(preferences_primitive_validate('float', 123.456));
  }

  /**
   * Asserts not float.
   */
  public function testNoFloat() {
    $this->assertFalse(preferences_primitive_validate('float', 1));
  }

  /**
   * Asserts true when null.
   */
  public function testNull() {
    $this->assertTrue(preferences_primitive_validate('int', NULL));
  }

  /**
   * Asserts string.
   */
  public function testString() {
    $this->assertTrue(preferences_primitive_validate('string', 'aoeui'));
  }

  /**
   * Asserts not string.
   */
  public function testNotString() {
    $this->assertFalse(preferences_primitive_validate('string', ['aoeui']));
  }

}

