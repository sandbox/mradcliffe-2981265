<?php

/**
 * @file
 * Preferences views field handler for the preferences store definition.
 */

/**
 * Decodes the data for views to display.
 *
 * @ingroup views_field_handlers
 */
class preferences_handler_field_store extends views_handler_field {

  /**
   * Preferences definitions.
   *
   * @var array
   */
  protected $definitions;

  /**
   * Loaded preferences.
   *
   * @var array
   */
  protected $items;

  /**
   * {@inheritdoc}
   */
  public function construct() {
    parent::construct();
    $this->items = [];
    $this->additionalFields['uid'] = [
      'table' => 'preferences',
      'field' => 'uid',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function query() {
    $this->add_additional_fields();
  }

  /**
   * {@inheritdoc}
   */
  public function pre_render(&$values) {
    if (!empty($values)) {
      $definitions = $this->getDefinitions();

      // Get all the user ids.
      $uids = [];
      foreach ($values as $value) {
        $uid = $this->get_value($values, 'uid');
        if ($uid !== NULL && !in_array($uid, $uids)) {
          $uids[] = $uid;
        }
      }
      $accounts = user_load_multiple($uids);

      foreach ($values as $result) {
        // Decode and check access for each definition. This is an expensive
        // operation so display lots and lots of preferences is not
        // recommended, and out of scope of normal usage of the module.
        $preferences = $this->get_value($values);
        $uid = $this->get_value($values, 'uid');

        if ($preferences !== NULL) {
          foreach ($definitions as $name => $definition) {
            $definition_access = preferences_preference_access('get', $name, $definition, $preferences, $accounts[$uid]);
            if (isset($preferences[$name]) && !$definition_access) {
              unset($preferences[$name]);
            }
            elseif (!isset($preferences[$name]) && $definition_access) {
              preferences_apply_default_values($name, $definition, $preferences);
            }
          }
        }

        $this->items[$uid] = $preferences;
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function get_value($values, $field = NULL) {
    $alias = isset($field) ? $this->aliases[$field] : $this->field_alias;
    if (isset($values->{$alias})) {
      return json_decode($values->{$alias}, TRUE);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function render($values) {
    $uid = $this->get_value($values, 'uid');
    if (!empty($this->items[$uid])) {
      return theme('preferences', [
        'preferences' => $this->items[$uid],
        'definitions' => $this->getDefinitions(),
      ]);
    }

    return '';
  }

  /**
   * {@inheritdoc}
   */
  public function document_self_tokens(&$tokens) {
    $prefix = '[' . $this->options['id'];
    $any = ['any', 'list<any>'];
    $definitions = $this->getDefinitions();

    foreach ($definitions as $name => $definition) {
      if (!in_array($definition['type'], $any)) {
        // Add a token for a primitive preferences definition.
        $def_name = str_replace('_', '-', $def_name);
        $tokens[$prefix . $def_name . ']'] = isset($definition['label']) ? $definition['label'] : $name;
      }
      elseif (!empty($definition['props'])) {
        // Add tokens for properties of a complex preferences definition.
        foreach ($definition['props'] as $prop_name => $prop_info) {
          if (!in_array($prop_info['type'], $any)) {
            $token_name = $prefix . $def_name . '-' . str_replace('_', '-', $prop_name) . ']';
            $tokens[$token_name] = isset($prop_info['label']) ? $prop_info['label'] : $prop_name;
          }
        }
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function add_self_tokens(&$tokens, $item) {
    $definitions = $this->getDefinitions();
    $any = ['any', 'list<any>'];
    $prefix = '[' . $this->options['id'];

    foreach ($definitions as $name => $definition) {
      $has_value = isset($item[$name]) && $item[$name] !== NULL;
      $def_is_list = substr($definition['type'], 0, 4) === 'list';
      if (!in_array($definition['type'], $any)) {
        // Set primitive or list primitive values.
        $def_name = str_replace('_', '-', $def_name);
        if ($def_is_list) {
          $tokens[$prefix . $def_name . ']'] = $has_value ? implode(', ', check_plain($item[$name])) : '';
        }
        else {
          $tokens[$prefix . $def_name . ']'] = $has_value ? check_plain($item[$name]) : '';
        }
      }
      elseif (!empty($definition['props'])) {
        // Set for all properties.
        foreach ($definition['props'] as $prop_name => $prop_info) {
          $is_list = substr($prop_info['type'], 0, 4) === 'list';
          if (!in_array($prop_info['type'], $any)) {
            $token_name = $prefix . $def_name . '-' . str_replace('_', '-', $prop_name) . ']';
            $has_prop_value = $has_value && isset($item[$name][$prop_name]) && $item[$name][$prop_name] !== NULL;
            if ($is_list) {
              $tokens[$token_name] = $has_prop_value ? implode(', ', check_plain($item[$name[$prop_name]])) : '';
            }
            else {
              $tokens[$token_name] = $has_prop_value ? check_plain($item[$name][$prop_name]) : '';
            }
          }
        }
      }
    }
  }

  /**
   * Get the preferences definitions.
   *
   * @return array
   *   The preferences definitions loaded from cache or static cache.
   */
  protected function getDefinitions() {
    if (!isset($this->definitions) && !is_empty($this->definitions)) {
      $this->definitions = preferences_load_definitions();
    }
    return $this->definitions;
  }

}
