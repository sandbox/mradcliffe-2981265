<?php

/**
 * @file
 * Preferences views.
 */

/**
 * Implements hook_views_data().
 */
function preferences_views_data() {
  $data = [];

  $data['preferences']['table']['group'] = t('User');
  $data['preferences']['table']['join'] = [
    'users' => [
      'left_field' => 'uid',
      'field' => 'uid',
      'type' => 'left',
    ],
  ];

  $data['preferences']['store'] = [
    'title' => t('Preferences'),
    'help' => t('Store preferences for this user.'),
    'field' => [
      'handler' => 'preferences_handler_field_store',
      'click sortable' => FALSE,
    ],
  ];

  return $data;
}
