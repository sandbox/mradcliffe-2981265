<?php

/**
 * @file
 * Preferences template.
 *
 * A simple way to render preferences that may not be very useful.
 *
 * Variables:
 *   - $preferences: The preferences array.
 *   - $uid: User Identifier of the user who owns the preferences.
 *   - $definitions: Definitions array.
 *   - $render: The rendered item list.
 *   - $items: Raw item list.
 */
?>
<?php print $render; ?>
