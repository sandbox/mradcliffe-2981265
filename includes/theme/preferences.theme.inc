<?php

/**
 * @file
 * Preferences theme definitions.
 */

/**
 * Template preprocess callback for preferences.
 *
 * @param array &$variables
 *   Theme variables to preprocess:
 *     - preferences: the loaded preferences.
 *     - uid: the user ID.
 *     - definitions: the preferences definitions.
 */
function template_preprocess_preferences(array &$variables) {
  $variables['items'] = [];
  $variables['render'] = '';
  $any_types = ['any', 'list<any>'];

  if (!empty($variables['definitions'])) {
    foreach ($variables['definitions'] as $name => $definition) {
      $preference = isset($variables['preferences'][$name]) ? $variables['preferences'][$name] : '';
      $def_label = isset($definition['label']) ? $definition['label'] : '';

      if (!in_array($definition['type'], $any_types)) {
        // Adds items for primitive values.
        $is_list = substr($definition['type'], 0, 4) === 'list';
        if ($is_list) {
          $def_item = [
            'data' => '<span class="definition-label">' . $def_label . '</span>',
            'children' => [],
            'attributes' => [
              'class' => ['definition-item', 'definition-item--list'],
            ],
          ];

          if (!empty($preference[$name])) {
            foreach ($preference[$name] as $list_value) {
              $def_item['children'][] = '<span class="definition-item definition-item--primitive">' . check_plain($list_value) . '</span>';
            }
          }
          $variables['items'][] = $def_item;
        }
        else {
          $variables['items'][] = [
            'data' => '<span class="definition-label">' . $def_label . ': </span><span class="definition-item definition-item--primitive">' . check_plain($preference) . '</span>',
            'class' => ['definition-item', 'definition-item--' . $definition['type']],
          ];
        }
      }
      elseif (!empty($definition['props'])) {
        $def_item = [
          'data' => '<span class="definition-label">' . $def_label . '</span>',
          'children' => [],
          'attributes' => [
            'class' => ['definition-item', 'definition-item--any'],
          ],
        ];

        foreach ($definitions['props'] as $prop_name => $prop_info) {
          $prop = isset($preference[$prop_name]) ? $preference[$prop_name] : '';
          $prop_label = isset($prop_info['label']) ? $prop_info['label'] : '';

          if (!in_array($prop_info['type'], $any_types)) {
            $prop_is_list = substr($definition['type'], 0, 4) === 'list';
            if ($prop_is_list) {
              $prop_item = [
                'data' => '<span class="definition-property-label">' . $prop_label . '</span>',
                'children' => [],
                'attributes' => [
                  'class' => ['definition-property-item', 'definition-property-item--list'],
                ],
              ];

              if (!empty($prop)) {
                foreach ($prop as $list_value) {
                  $prop_item['children'][] = '<span class="definition-property-list-item">' . check_plain($list_value) . '</span>';
                }
              }

              $def_item['children'][] = $prop_item;
            }
            else {
              $def_item['children'][] = [
                'data' => '<span class="definition-property-label">' . $prop_label . ': </span><span class="definition-property-item definition-property-item--primitive">' . check_plain($prop) . '</span>',
              ];
            }
          }
        }

        $variables['items'][] = $def_item;
      }
    }
    $variables['render'] = theme('item_list', $variables['items']);
  }
}
