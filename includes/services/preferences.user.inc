<?php

/**
 * @file
 * Preferences User Services Resources.
 */

/**
 * Preferences user resource access callback.
 *
 * @param int $uid
 *   The user account to fetch preferences for.
 * @param string $preference_name
 *   (Optional) The preference to get i.e. "global".
 * @param int $with_definition
 *   (Optional) The preferences relationshp includes a "withDefinition"
 *   parameter. Defaults to 0. Unused for access.
 *
 * @return bool
 *   TRUE if the current user has access to the get preferences.
 */
function preferences_user_resource_access($uid, $preference_name = 'global', $with_definition = 0) {
  global $user;

  $account = user_load($uid);
  if (!$account) {
    throw new \ServicesException('Preference not found', 404);
  }

  if (isset($preference_name)) {
    try {
      $definition = preferences_load_definition($preference_name);
    }
    catch (\Exception $e) {
      throw new \ServicesException($e->getMessage(), 500);
    }

    if (!$definition) {
      throw new \ServicesException('Preference not found', 404);
    }
  }

  return $user->uid === $account->uid || user_access('administer users');
}

/**
 * Preferences user fetch resource callback.
 *
 * @param int $uid
 *   The user account identifier to load preferences for.
 * @param string $preference_name
 *   (Optional) The preferences definition name.
 * @param int $withDefinition
 *   (Optional) The preferences relationshp includes a "withDefinition"
 *   parameter. Defaults to 0.
 *
 * @return array
 *   An array of preferences keyed by preference name or the preferences object
 *   associated with the given preference name.
 */
function preferences_user_fetch_resource($uid, $preference_name = 'global', $withDefinition = 0) {
  $account = user_load($uid);
  $preferences = preferences_load($preference_name, $account);
  $withDefinition = (int) $withDefinition;

  if ($withDefinition) {
    $preferences[$preference_name]['_definition'] = preferences_load_definition($preference_name);
    $definition = preferences_load_definition($preference_name);

    $preferences[$preference_name]['_definition'] = [
      'type' => $definition['type'],
      'label' => isset($definition['label']) ? $definition['label'] : null,
      'props' => null,
    ];
    $def_info = &$preferences[$preference_name]['_definition'];
    if ($definition['type'] !== 'any') {
      $def_info['default_value'] = isset($definition['default_value']) ? $definition['default_value'] : null;
    }
    else {
      if (!empty($definition['props'])) {
        foreach ($definition['props'] as $name => $prop_info) {
          $def_info['props'][$name] = [
            'type' => $prop_info['type'],
            'label' => isset($prop_info['label']) ? $prop_info['label'] : null,
            'default_value' => isset($prop_info['default_value']) ? $prop_info['default_value'] : null,
          ];
        }
      }
    }
  }

  return $preferences;
}

/**
 * Preferences user save resource callback.
 *
 * @param int $uid
 *   The user account identifier to load preferences for.
 * @param string $preference_name
 *   The preferences definition name.
 * @param array $data
 *   Data to set.
 */
function preferences_user_save_resource($uid, $preference_name, array $data) {
  $ret = [];
  $account = user_load($uid);

  $preferences = preferences_load(NULL, $account);

  try {
    $modified = preferences_set($preference_name, $data, $preferences, $account);
    $ret = preferences_save($modified, $account);

    if (!$ret) {
      return [];
    }

    // Keeps the response similar to GET request.
    return [$preference_name => $ret[$preference_name]];
  }
  catch (\PDOException $e) {
    throw new \ServicesException(t('An error occurred saving preferences'), 500);
  }
  catch (\InvalidArgumentException $e) {
    throw new \ServicesException(t('Preferences are not valid'), 406);
  }
  catch (\Exception $e) {
    throw new \ServicesException(t('An error occurred setting preferences'), 500);
  }
}
