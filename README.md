# Preferences

Preferences provides a framework and storage of per-user preferences (or settings) defined by other modules.

Preference definitions can be as simple or complex, and are validated before being saved as one JSON string (or object) for easy retrieval.
The preference object is not loaded automatically for performance reasons.

The main use case is to provide the arbitrary data store to front-end applications that may not necessarily know about Drupal and the data may be easily migrated to/from Drupal in a pseudo-schemaless manner.

## Potential future uses

* Rules condition to compare against.
* DBAL improvements for query JSON-stored properties for database drivers that support JSON.
